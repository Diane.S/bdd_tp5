import java.awt.*;
import java.awt.event.*;
import java.sql.*;

/**
 * This is a skeleton for realizing a very simple database user interface in java.
 * The interface is an Applet, and it implements the interface ActionListener.
 * If the user performs an action (for example he presses a button), the procedure actionPerformed
 * is called. Depending on his actions, one can implement the database connection (disconnection),
 * querying or insert.
 *
 * @author zmiklos
 *
 */
public class DatabaseUserInterface extends java.applet.Applet implements ActionListener {

 private TextField mStat, m1, m2, m3, m4, m5, m6, m7, m8, m9;
 TextArea mRes;
 private Button b1, b2, b3, b4, b5;
 private static final long serialVersionUID = 1L;

//JDBC driver name and database URL
 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
 static final String DB_URL = "jdbc:mysql://mysql.istic.univ-rennes1.fr/base_17019624";

 //  Database credentials
 static final String USER = "user_17019624";
 static final String PASS = "Bdd2019tp5";

 static Connection conn = null;
 static Statement stmt = null;

 /**
  * This procedure is called when the Applet is initialized.
  *
  */
 public void init ()
 {
	 /**
	  * Definition of text fields
	  */
     //m1 = new TextField(80);
     //m1.setText("What are you going to do when the light is:");
     //m1.setEditable(false);
     mStat= new TextField(50);
     mStat.setEditable(false);
     m1 = new TextField(150);
     m2 = new TextField(150);
     m3 = new TextField(150);
     m4 = new TextField(150);
     m5 = new TextField(150);
     m6 = new TextField(150);
     m7 = new TextField(150);
     m8 = new TextField(145);
     m9 = new TextField(145);
     mRes = new TextArea(10,150);
     mRes.setEditable(false);



     /**
      * First we define the buttons, then we add to the Applet, finally add and ActionListener
      * (with a self-reference) to capture the user actions.
      */
     add(new Label("                                                                                                                  ", Label.LEFT));
     Label l1 = new Label("BIBLIOGRAPHIE DE L'ESIR", Label.CENTER);
     l1.setBounds(20,0,200,20);
     add(l1);
     add(new Label("                                                                                                                  ", Label.LEFT));
     add(new Label("Rechercher des livres :", Label.LEFT));
     add(new Label("                                                                                                                                                                                                                                                                        ", Label.LEFT));
     add(new Label("Titre", Label.LEFT));
     add(m1);
     add(new Label("Auteur", Label.LEFT));
     add(m2);
     add(new Label("Collection", Label.LEFT));
     add(m3);
     add(new Label("Cours", Label.LEFT));
     add(m4);
     add(new Label("Parcours", Label.LEFT));
     add(m5);
     add(new Label("Semestre (1 ou 2)", Label.LEFT));
     add(m6);
     add(new Label("Annee", Label.LEFT));
     add(m7);
     b1 = new Button("RECHERCHER");
     add(b1) ;
     add(mRes);
     mRes.setText("Livres trouvés ...");
     add(new Label("                                                                                 ", Label.LEFT));
     add(new Label("Espace Enseignant", Label.CENTER));
     add(new Label("                                                                                 ", Label.LEFT));
     add(new Label("Inserer une référence (Titre, Auteur, Collection, Cours, Parcours, Semestre, Annee)", Label.LEFT));
     add(m8);
     b2 = new Button("INSERER");
     add(b2) ;
     add(new Label("Mettre à jour des informations (cours, livres ...) -- En SQL", Label.LEFT));
     add(m9);
     b3 = new Button("UPDATE");
     add(b3);
     b4 = new Button("DECONNECTION");
     b5 = new Button("CONNEXION");
     add(new Label("                                                                                       ", Label.LEFT));
     add(b5);
     add(b4);
     add(new Label("                                                                                       ", Label.LEFT));
     b1.addActionListener(this);
     b2.addActionListener(this);
     b3.addActionListener(this);
     b4.addActionListener(this);
     b5.addActionListener(this);

     add(mStat);
     setStatus("Waiting for user actions.");
     setSize(new Dimension(1250, 1000));
     connectToDatabase();
 }


 /**
  * This procedure is called upon a user action.
  *
  *  @param event The user event.
  */
  public void actionPerformed(ActionEvent event)
 {

	 // Extract the relevant information from the action (i.e. which button is pressed?)
	 Object cause = event.getSource();

	 // Act depending on the user action
	 // Button RECHERCHER
     if (cause == b1)
     {
        queryDatabase();
     }

     // Button INSERER
     if (cause == b2)
     {
    	 insertDatabase();
     }

     //Button UPDATE
     if (cause == b3)
     {
    	 updateDatabase();
     }

     //Button DISCONNECT
     if (cause == b4)
     {
         disconnectFromDatabase();
     }

     //Button DISCONNECT
     if (cause == b5)
     {
    	 connectToDatabase();
     }

 }


/**
 * Set the status text.
 *
 * @param text The text to set.
 */
private void setStatus(String text){
	    mStat.setText("Status: " + text);
  }

/**
 * Procedure, where the database connection should be implemented.
 */
private void connectToDatabase(){
	try{
		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Connecting to database...");
	    conn = DriverManager.getConnection(DB_URL,USER,PASS);
		setStatus("Connected to the database");
	} catch(Exception e){
		System.err.println(e.getMessage());
		setStatus("Connection failed");
	}
}


/**
 * Procedure, where the database connection should be implemented.
 */
private void disconnectFromDatabase(){
	try{
		if(conn!=null){
			conn.close();
			setStatus("Disconnected from the database");
		}
	} catch(Exception e){
		System.err.println(e.getMessage());
		setStatus("Disconnection failed");
	}
}

/**
 * Execute a query and display the results. Implement the database querying and the
 * display of the results here
 */
private void queryDatabase(){
	try {
		setStatus("Querying the database");
		System.out.println("Creating statement...");
		stmt = conn.createStatement();
		String where = "";
		String letitre = m1.getText();
		if (!letitre.isEmpty()){
			if (where == ""){
				where = where + " WHERE titre LIKE \'%" + letitre + "%\'";
			}
			else {
				where = where + " AND titre LIKE \"%" + letitre + "%\"";
			}
		}
		String lauteur = m2.getText();
		if (!lauteur.isEmpty()){
			if (where == ""){
				where = where + " WHERE auteur LIKE \"%" + lauteur + "%\"";
			}
			else {
				where = where + " AND auteur LIKE \"%" + lauteur + "%\"";
			}
		}
		String lacollection = m3.getText();
		if (!lacollection.isEmpty()){
			if (where == ""){
				where = where + " WHERE collection LIKE \"%" + lacollection + "%\"";
			}
			else {
				where = where + " AND collection LIKE \"%" + lacollection + "%\"";
			}
		}
		String lecours = m4.getText();
		if (!lecours.isEmpty()){
			if (where == ""){
				where = where + " WHERE cours LIKE \"%" + lecours + "%\"";
			}
			else {
				where = where + " AND cours LIKE \"%" + lecours + "%\"";
			}
		}
		String leparcours = m5.getText();
		if (!leparcours.isEmpty()){
			if (where == ""){
				where = where + " WHERE parcours LIKE \"%" + leparcours + "%\"";
			}
			else {
				where = where + " AND parcours LIKE \"%" + leparcours + "%\"";
			}
		}
		String lesemestre = m6.getText();
		if (!lesemestre.isEmpty()){
			if (where == ""){
				where = where + " WHERE semestre LIKE \"%" + lesemestre + "%\"";
			}
			else {
				where = where + " AND semestre LIKE \"%" + lesemestre + "%\"";
			}
		}
		String lannee = m7.getText();
		if (!lannee.isEmpty()){
			if (where == ""){
				where = where + " WHERE annee LIKE \"%" + lannee + "%\"";
			}
			else {
				where = where + " AND annee LIKE \"%" + lannee + "%\"";
			}
		}
		String sql = "SELECT * FROM Bibliographie" + where + ";";
	    ResultSet rs = stmt.executeQuery(sql);

	    //STEP 5: Extract data from result set
	    mRes.setText("");
	    while(rs.next()){
	       //Retrieve by column name
	       String titre  = rs.getString("titre");
	       String auteur  = rs.getString("auteur");
	       String collection  = rs.getString("collection");
	       String cours  = rs.getString("cours");
	       String parcours  = rs.getString("parcours");
	       String semestre  = rs.getString("semestre");
	       String annee  = rs.getString("annee");

	       //Display values
	       mRes.append("Titre: " + titre);
	       mRes.append(", Auteur: " + auteur);
	       mRes.append(", Collection: " + collection);
	       mRes.append(", Cours: " + cours);
	       mRes.append(", Parcours: " + parcours);
	       mRes.append(", Semestre: " + semestre);
	       mRes.append(", Annee: " + annee + ".\n");
	    }
	}
	catch (Exception e){
		System.err.println(e.getMessage());
		setStatus("Query failed");
	}
}

/**
 * Insert tuples to the database.
 */
private void insertDatabase(){
	try{
		String titre = "";
		String auteur = "";
		String collection = "";
		String cours = "";
		String parcours = "";
		String semestre = "";
		String annee = "";
		int fin = 0;
		int i = 0;
		int j = 0;
		while (j < 7){
			switch (j){
			case 0 :
				if (m8.getText().charAt(i) == ','){
					i++;
					j++;
				}
				titre = titre + m8.getText().charAt(i);
				break;
			case 1 :
				if (m8.getText().charAt(i) == ','){
					i++;
					j++;
				}
				auteur = auteur + m8.getText().charAt(i);
				break;
			case 2 :
				if (m8.getText().charAt(i) == ','){
					i++;
					j++;
				}
				collection = collection + m8.getText().charAt(i);
				break;
			case 3 :
				if (m8.getText().charAt(i) == ','){
					i++;
					j++;
				}
				cours = cours + m8.getText().charAt(i);
				break;
			case 4 :
				if (m8.getText().charAt(i) == ','){
					i++;
					j++;
				}
				parcours = parcours + m8.getText().charAt(i);
				break;
			case 5 :
				semestre = semestre + m8.getText().charAt(i);
				i+=2;
				j++;
				System.out.println(m8.getText().charAt(i));
				break;
			case 6 :
				annee = annee + m8.getText().substring(i, i+4);
				j++;
				break;
			}
			i++;
		}
		setStatus("Inserting to the database");
	    stmt = conn.createStatement();
	    String sql = "INSERT IGNORE INTO Bibliographie " + "VALUES ( \"" + titre + "\", \"" + auteur + "\", \'" + collection + "\', \'" + cours + "\', \'" + parcours + "\', \'" + semestre + "\', \'" + annee + "\');";
	    stmt.executeUpdate(sql);
	    setStatus("Inserted to the database");
		} catch(Exception e){
			System.err.println(e.getMessage());
			setStatus("Insertion failed");
		}

}

private void updateDatabase(){
	try{
		setStatus("Update the database");
	    stmt = conn.createStatement();
	    String sql = m4.getText();
	    stmt.executeUpdate(sql);
		} catch(Exception e){
			System.err.println(e.getMessage());
			setStatus("Update failed");
		}
}

}
